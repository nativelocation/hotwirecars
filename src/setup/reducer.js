import { combineReducers } from 'redux-immutable';
import carsReducer from 'containers/HomePage/reducer';

export default function createReducer() {
  return combineReducers({
    cars: carsReducer,
  });
}
