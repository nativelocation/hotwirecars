import carsSagas from 'containers/HomePage/sagas';

export default function* rootSaga() {
  yield [
    ...carsSagas,
  ];
}
