import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import HomePage from 'containers/HomePage';
import { Layout } from 'components';

function Router() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route path="/" component={HomePage} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default Router;
