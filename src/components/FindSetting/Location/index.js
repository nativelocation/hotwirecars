import React from 'react';
import styles from './styles';
import { TextField } from 'material-ui';

class Location extends React.Component {

  render() {
    const { onLocation } = this.props;
    return (
      <div style={styles.container}>
        <label style={styles.title}>Location</label>
        <TextField onChange={onLocation} style={styles.input}/>
      </div>
    );
  }
}

export default Location;
