const styles = {
  container: {
    border:'1px solid black',
    backgroundColor: '#ffffff',
    padding: '0 10px',
    width: '35%',
  },
  button:{
    fontSize: 40,
    backgroundColor: 'red',
    color: 'white',
    marginTop: '25px',
    marginBottom: '5px',
    marginLeft: '80px'
  },
};

export default styles;
