import React, { PropTypes } from 'react';
import styles from './styles';
import Location from './Location';
import SettingDate from './SettingDate';

class FindSetting extends React.Component {
  
  onLocation = (e, newValue) => {
    this.setState({
      location: newValue,
    });    
  }
  onChangeDatePicker = (e) => {
    this.setState({
      datePicker: e,
    });
  }
  onChangeTimePicker = (e) => {
    this.setState({
      timePicker: e,
    });
  }
  onChangeDateDrop = (e) => {
    this.setState({
      dateDrop: e,
    });
  }
  onChangeTimeDrop = (e) => {
    this.setState({
      timeDrop: e,
    });
  }
  findCar = () => {
    this.props.findCar(
      "?apikey=u6cysn5b9mj4hdbcwbpppfst&format=json&dest=" +
       this.state.location + "&startdate=" + this.state.datePicker + "&enddate=" + this.state.dateDrop +
        "&pickuptime=" + this.state.timePicker + "&dropofftime=" + this.state.timeDrop);
}

  state = {
    datePicker: '01/01/2017',
    timePicker: '12:00',
    dateDrop: '01/02/2017',
    timeDrop: '12:00',
    location: 'LAX',
  };

  render() {
    // const { onChangeDatePicker, onChangeTimePicker, onChangeDateDrop, onChangeTimeDrop, findCar } = this.props
    return (
      <div style={styles.container}>
        <Location onLocation={this.onLocation} />
        <SettingDate title={'Pick up date'} date={this.state.datePicker} time={this.state.timePicker} onChangeDate={this.onChangeDatePicker} onChangeTime={this.onChangeTimePicker} />
        <SettingDate title={'Drop off date'} date={this.state.dateDrop} time={this.state.timeDrop} onChangeDate={this.onChangeDateDrop} onChangeTime={this.onChangeTimeDrop}  />
        <button style={styles.button} onClick={this.findCar}>Find a Car</button>
      </div>
    );
  }
}

export default FindSetting;
