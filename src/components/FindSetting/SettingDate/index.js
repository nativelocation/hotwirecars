import React, { PropTypes } from 'react';
import styles from './styles';

import moment from 'moment';
import Kronos from './TimePicker';

class SettingDate extends React.Component {

  render() {
    const { title, date, time, onChangeDate, onChangeTime } = this.props
    return (
      <div>
        <label style={styles.title}>{title}</label>
        <div style={styles.setting}>
          <Kronos
            date={date}
            returnAs={'STRING'}
            format={'MM/DD/YYYY'}
            onChangeDateTime={onChangeDate}
          />
          <Kronos
            time={time}
            returnAs={'STRING'}
            format={'HH:MM'}
            onChangeDateTime={onChangeTime}
          />
        </div>
      </div>
    );
  }
}

export default SettingDate;
