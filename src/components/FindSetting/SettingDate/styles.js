const styles = {
  container: {
    backgroundColor: '#ffffff',
  },
  title:{
    display: 'block',
    fontSize: 30,
    margin: '5px 0',
  },
  setting:{
    display: 'flex',
    justifyContent: 'space-between',
  },
  date:{
    fontSize: 20,
    width: '60%',
  },
  time:{
    width: '20%'
  },
};

export default styles;
