import React, { PropTypes } from 'react';
import styles from './styles';

class SearchItem extends React.Component {

  render() {
    const { MileageDes, LocationDes, TotalPrice, DailyRate, CarTypeCode } = this.props.param;
    return (
      <div style={styles.container}>
        <div style={styles.row}>
          <label style={styles.title}>{LocationDes}</label>
          <label style={styles.price}>{DailyRate}</label>
        </div>
        <div style={styles.row}>
          <label style={styles.destination}>{CarTypeCode}</label>
          <label style={styles.period}>{'and up'}</label>
        </div>
        <div style={styles.row}>
          <label style={styles.distance}>{MileageDes}</label>
          <label style={styles.totalprice}>{TotalPrice}</label>
        </div>
      </div>
    );
  }
}

export default SearchItem;
