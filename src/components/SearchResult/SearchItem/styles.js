const styles = {
  container: {
    border:'1px solid black',
    backgroundColor: '#ffffff',
    padding: '0 10px',
    marginBottom: 20,
  },
  row:{
    display: 'flex',
    justifyContent: 'space-between',
  },
  title:{
    fontSize: 30,
    fontWeight: 'bold',
  },
  price:{
    color: 'red',
    fontSize: 25,
    fontWeight: 'bold',
  },
  destination: {
    fontSize: 20,
  },
  period: {
    fontSize: 18,
  },
  distance:{
    fontSize: 20,
  },
  totalprice:{
    fontSize: 20,
  }, 
};

export default styles;
