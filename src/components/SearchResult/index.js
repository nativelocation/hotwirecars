import React, { PropTypes } from 'react';
import styles from './styles';
import SearchItem from './SearchItem';

class SearchResult extends React.Component {

  render() {
    const { result } = this.props;
    return (
      <div style={styles.container}>
        {result.map((item, index) => (
          <SearchItem key={index} param={item}/>
        ))}
      </div>
    );
  }
}

export default SearchResult;
