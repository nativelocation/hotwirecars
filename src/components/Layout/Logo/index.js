import React, { PropTypes } from 'react';
import styles from './styles';

function Logo({ onRequestChange }) {
  return (
    <div style={styles.logoWrapper} >
      Search Hotwire Cars!!
    </div>
  );
}

export default Logo;
