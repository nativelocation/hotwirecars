import colors from 'styles/colors';

const styles = {
  logoWrapper: {
    margin: '20px 0 0 20px' ,
    display: 'inline-block',
    color: colors.white,
    textDecoration: 'none',
    fontSize: '35px',
    fontFamily: 'sans-serif',
    fontWeight: 'bold',
    lineHeight: '35px',
  },
};

export default styles;
