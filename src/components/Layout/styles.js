import colors from 'styles/colors';

const styles = {
  contentWrapper: {
    backgroundColor: colors.white,
    flex: 1,
    overflow: 'auto',
    padding:'20px 20px',
  },
};

export default styles;
