import colors from 'styles/colors';

const styles = {
  header: {
    backgroundImage: 'url(/assets/image/header.png)',
    height: '258px',
  },
};

export default styles;
