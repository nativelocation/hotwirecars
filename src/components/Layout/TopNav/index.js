import React from 'react';
import { Responsive } from 'components';
import Logo from '../Logo';
import styles from './styles';

function TopNav() {
  return (
    <div style={styles.header}>
      <Responsive size="desktop">
        <Logo />
      </Responsive>
      <Responsive size="mobile" />
    </div>
  );
}

export default TopNav;
