import React, { PropTypes } from 'react';
import TopNav from './TopNav';

import styles from './styles';

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node,
  };

  static defaultProps = {
    children: null,
  };

  render() {
    const { children } = this.props;
    return (
      <div>
        <TopNav />
        <div style={styles.contentWrapper}>
          {children}
        </div>
      </div>
    );
  }
}

export default Layout;
