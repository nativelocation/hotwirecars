import { API } from 'setup/config';

export function getCarItems(param) {
  console.log(param);
  return fetch(`https://cors-anywhere.herokuapp.com/${API}${param}`, {
    mode: 'cors',
  })
    .then(res => res.json())
    .catch(function(error) {  
      console.log('Request failed', error)  
    });
}
