import { createSelector } from 'reselect';

const carsSelector = state => state.get('cars');
const carItemsSelector = createSelector([carsSelector], cars => cars.get('carItems'));

export {
  carsSelector,
  carItemsSelector,
};
