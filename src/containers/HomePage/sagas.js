import { delay } from 'redux-saga';
import { put, call, takeLatest } from 'redux-saga/effects';

import {
  FETCH_CARS_START,
} from './constants';

import {
  carsFetched,
} from './actions';

import { getCarItems } from './api';

function* asyncFetchOrders(payload) {
  const response = yield call(getCarItems, payload.payload.param);
  // console.log(response);
  yield put(carsFetched(response));
  
}

export function* sagaWatcher() {
  yield takeLatest(FETCH_CARS_START, asyncFetchOrders);
}

export default [
  sagaWatcher(),
];
