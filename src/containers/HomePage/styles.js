const styles = {
  container: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  addCircleButton: {
    position: 'absolute',
    right: 20,
    bottom: 20,
  },
};

export default styles;
