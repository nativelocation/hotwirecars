import { fromJS, List } from 'immutable';
import { handleActions } from 'redux-actions';

import {
  FETCH_CAR_SUCCESS,
  SET_CARS
} from './constants';

const initialState = fromJS({
  carItems: [],
});

export default handleActions({
  [SET_CARS]: (state, action) => state.updateIn(['carItems'], list => List(action.payload.Result.map(item =>
      ({
        MileageDes: item.MileageDescription,
        LocationDes: item.LocationDescription,
        TotalPrice: item.TotalPrice,
        DailyRate: item.DailyRate,
        CarTypeCode: item.CarTypeCode,
      }),
    )))
}, initialState);

