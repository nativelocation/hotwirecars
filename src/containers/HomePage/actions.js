import { createAction } from 'redux-actions';
import {
  FETCH_CARS_START,
  FETCH_CARS_SUCCESS,
  FETCH_CARS_FAILURE,
  SET_CARS,
} from './constants';

export const fetchCars = createAction(
  FETCH_CARS_START,
  (param) => ({ param }),
);

export const carsFetched = createAction(
  SET_CARS,
);