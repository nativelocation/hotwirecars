export const FETCH_CARS_START = 'cars/FETCH_CARS_START';
export const FETCH_CARS_SUCCESS = 'cars/FETCH_CARS_SUCCESS';
export const FETCH_CARS_FAILURE = 'cars/FETCH_CARS_FAILURE';
export const SET_CARS = 'cars/SET_CARS';
