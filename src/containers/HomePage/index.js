import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { RaisedButton } from 'material-ui';

import { Layout } from 'components';
import { FindSetting } from 'components';
import { SearchResult } from 'components';
import { carItemsSelector } from './selectors';
import { fetchCars } from './actions';

import styles from './styles';

const mapStateToProps = state => ({
  carItems: carItemsSelector(state),
});

const mapDispatchToProps = dispatch => ({
  fetchCars: (param) => dispatch(fetchCars(param)),
});

class HomePage extends React.Component {
  componentDidMount() {
    //this.props.fetchCars("?apikey=u6cysn5b9mj4hdbcwbpppfst&format=json&dest=LAX&startdate=07/12/2017&enddate=07/13/2017&pickuptime=23:00&dropofftime=11:00");
  }

  render() {
    const { carItems, fetchCars } = this.props;
    return (
      <div style={styles.container}>
        <FindSetting findCar={fetchCars} />
        <SearchResult result={carItems} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
